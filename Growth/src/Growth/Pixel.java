package Growth;

import java.util.Random;

public class Pixel {

	private Random generator = new Random();
	public int act = 0;
	public int prev = 0;

	public int rek = 0;
	public int rek_prev = 0;

	public double dyslocationDensity = 0.0;

	public int R = 255;
	public int G = 255;
	public int B = 255;
	
	public int RR = -1;
	public int GG = -1;
	public int BB = -1;

	public Pixel() {
	}

	public Pixel(Pixel p) {
		this.prev = p.prev;
		this.act = p.act;
		this.rek = p.rek;
		this.rek_prev = p.rek_prev;
		this.dyslocationDensity = p.dyslocationDensity;
		this.R = p.R;
		this.G = p.G;
		this.B = p.B;
		this.RR = p.RR;
		this.GG = p.GG;
		this.BB = p.BB;
	}

	public void setRecristalizationStatus(int x) {
		this.rek = x;
		this.rek_prev = x;

		this.dyslocationDensity = 0.0;

		this.RR = generator.nextInt(255);
		this.GG = generator.nextInt(255);
		this.BB = generator.nextInt(255);
	}

	public void setPixel(int x) {
		this.prev = x;
		this.act = x;
		this.rek = 0;
		this.rek_prev = 0;

		this.dyslocationDensity = 0.0;

		this.R = 255;
		this.G = 255;
		this.B = 255;
		this.RR = -1;
		this.GG = -1;
		this.BB = -1;
	}

	public void setRandomColor(int x) {
		this.setPixel(x);

		this.R = generator.nextInt(255);
		this.G = generator.nextInt(255);
		this.B = generator.nextInt(255);
	}

	public void recolorRecristalization(Pixel p) {
		this.RR = p.RR;
		this.GG = p.GG;
		this.BB = p.BB;
	}

	public void recolor(Pixel p) {
		this.R = p.R;
		this.G = p.G;
		this.B = p.B;
	}

	public boolean colorMatch(Pixel p) {
		if (this.R==255 && this.G==255 && this.B==255) return false;
		if (this.R == p.R && this.G == p.G && this.B == p.B)
			return true;
		return false;
	}
		
	public boolean equals(Pixel p) {
	    return this.colorMatch(p);
	}
}
