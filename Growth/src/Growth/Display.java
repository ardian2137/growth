package Growth;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JRadioButton;

public class Display extends JFrame {

	private Board panel_1 = null;
	private JPanel contentPane;
	private String[] typesOfNeighborhood = new String[] {"Moore", "von Neuman", "Heksagonalnie lewo", "Heksagonalnie prawo", "Heksagonalnie random","Pentagonalnie random"};
	private JSpinner spinner_1;
	private String[] periodycznosc = new String[] {"z periodycznoscia","bez periodycznosci"};
	private String[] typyRozmieszczenia = new String[] {"R�wnomierny","Random z promieniem ko�a","Czysty random","Monte Carlo"};
	
	public Display() {
		System.out.println("Display::Display();");
		setForeground(UIManager.getColor("Button.highlight"));
		setBackground(Color.WHITE);
		setTitle("Modelowanie Wielkoskalowe - Rozrost ziaren");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 700);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
				
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		panel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				System.out.println("KeyPressed: "+e.getKeyCode()+", ts="+e.getWhen());
			}
			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println("keyReleased: "+e.getKeyCode()+", ts="+e.getWhen());
			}
		});
		
		final JComboBox comboBox_2 = new JComboBox(periodycznosc);
		comboBox_2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent argP) {
				Settings.boundaryCondition = comboBox_2.getSelectedIndex();
			}
		});
		
		final JComboBox comboBox = new JComboBox(typyRozmieszczenia);
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent argP) {
				Settings.placingType = comboBox.getSelectedIndex()+1;
			}
		});
		comboBox.setToolTipText("\r\n");
		panel.add(comboBox);
		panel.add(comboBox_2);
		JLabel lblPdzel = new JLabel("S�siedztwo:");
		panel.add(lblPdzel);
		
		final JComboBox comboBox_1 = new JComboBox();
		comboBox_1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent argP) {
				Settings.neighborhoodType = comboBox_1.getSelectedIndex();
			}
		});
		
		comboBox_1.setModel(new DefaultComboBoxModel(this.typesOfNeighborhood ));
		panel.add(comboBox_1);
		

		panel_1 = new Board();
		panel_1.setBackground(SystemColor.text);
		contentPane.add(panel_1, BorderLayout.CENTER);

	    new Thread(panel_1).start();  

	    this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
            	panel_1.refresh();
            }
        });
	    
		JButton btnGeneruj = new JButton("START");
		btnGeneruj.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Display.this.panel_1.clean();
				Display.this.panel_1.positionPlacement();
				Display.this.panel_1.refresh();
				Settings.isRunning = true;
			}
		});
		
		JLabel label_1 = new JLabel(" ");
		panel.add(label_1);
		
		JLabel lblIloZiaren = new JLabel("Ziarna:");
		panel.add(lblIloZiaren);
		
		final JSpinner spinner = new JSpinner();
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Settings.startingPoints = (Integer) spinner.getValue();
			}
		});
		spinner.setModel(new SpinnerNumberModel(Settings.startingPoints, 1, 360, 1));
		panel.add(spinner);
		
		JLabel label = new JLabel(" ");
		panel.add(label);
		
		JLabel lblCzas = new JLabel("Czas");
		panel.add(lblCzas);
		
		spinner_1 = new JSpinner();
		spinner_1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Settings.delay = (Integer) spinner_1.getValue();
			}
		});
		spinner_1.setModel(new SpinnerNumberModel(20, 1, 9999, 1));
		panel.add(spinner_1);
		
		JLabel lblMs = new JLabel("ms");
		panel.add(lblMs);
		
		JLabel label_2 = new JLabel(" ");
		panel.add(label_2);
		panel.add(btnGeneruj);
				
		JButton btnReset = new JButton("Czysc plansze");
		btnReset.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Display.this.panel_1.clean();
				Display.this.panel_1.refresh();
			}
		});
		panel.add(btnReset);
		
		JRadioButton rdbtnRekrystalizacja = new JRadioButton("Rekrystalizacja");
		rdbtnRekrystalizacja.setSelected(true);
		rdbtnRekrystalizacja.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(Settings.isRecrystalizationActive )
					Settings.isRecrystalizationActive = false;
				else
					Settings.isRecrystalizationActive = true;
			}
		});
		panel.add(rdbtnRekrystalizacja);		
	}
}
